# Seppiko Commons Mail
![Maven Central](https://img.shields.io/maven-central/v/org.seppiko.commons/commons-mail)

Jakarta Mail(Java Mail) 2.0 toolkit,support SMTP and IMAP.

```xml
<dependency>
  <groupId>org.seppiko.commons</groupId>
  <artifactId>commons-mail</artifactId>
  <version>${seppiko-commons-mail.version}</version>
</dependency>
```

## Jakarta Mail(Java Mail)

[Jakarta Mail home](https://eclipse-ee4j.github.io/mail/)

[Jakarta Mail javadoc](https://eclipse-ee4j.github.io/mail/docs/api/)

## Document

Properties `mail.encoding` will be set subject and text Encoding.

All Jakarta Mail properties see:
- [SMTP properties](https://eclipse-ee4j.github.io/mail/docs/api/com/sun/mail/smtp/package-summary.html#properties)
- [IMAP properties](https://eclipse-ee4j.github.io/mail/docs/api/com/sun/mail/imap/package-summary.html#properties)
- [POP3 properties](https://eclipse-ee4j.github.io/mail/docs/api/com/sun/mail/pop3/package-summary.html#properties)

## Example

```java
  public void jakartaMailSendTest() throws Throwable {
    Session session = Session.getInstance(props, null);
    JakartaMailSender sender = new JakartaMailSender(session);
    sender.setUser(username, password);
    MailMessage message = new MailMessage();
    message.setFrom(new InternetAddress(username));
    message.setTo(new InternetAddress[]{new InternetAddress("")});
    message.setSubject("");
    message.setText("");
    sender.send(message);
  }
```

```java
  public void jakartaMailReceiveTest() throws Throwable {
    Session session = Session.getInstance(props, null);
    JakartaMailReceiver receive = new JakartaMailReceiver(session);
    receive.setUser(username, password);
    ArrayList<MailMessage> messages = receive.receive("INBOX");
    for (MailMessage message: messages) {
      // read mail content
      System.out.println("From " + message.getFrom());
      System.out.println("To " + Arrays.toString(message.getTo()));
      System.out.println("Cc " + Arrays.toString(message.getCc()));
      System.out.println("Bcc " + Arrays.toString(message.getBcc()));
      System.out.println("ReplyTo " + message.getReplyTo());
      System.out.println("Subject " + message.getSubject());
      System.out.println(message.getText());
    }
  }
```

## License
This project is released under [Apache License,Version 2.0][alv2].

[alv2]: https://www.apache.org/licenses/LICENSE-2.0

## Sponsors

<a href="https://www.jetbrains.com/" target="_blank"><img src="https://seppiko.org/images/jetbrains.png" alt="JetBrians" width="100px"></a>
