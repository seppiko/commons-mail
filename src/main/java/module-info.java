/**
 * @author Leonard Woo
 */
module seppiko.commons.mail {
  requires jakarta.mail;
  requires com.sun.mail;

  exports org.seppiko.commons.mail;
}