/*
 * Copyright 2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.mail;

import jakarta.mail.Message.RecipientType;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import java.nio.charset.StandardCharsets;

/**
 * MimeMessage config helper
 *
 * @author Leonard Woo
 */
public class MimeMessageHelper {

  private final MimeMessage message;

  private boolean validateAddresses;

  private String encoding = StandardCharsets.UTF_8.name();

  public MimeMessageHelper(MimeMessage message) throws MessagingException {
    this.message = message;
    this.validateAddresses = false;
    this.createMimeMultiparts(message);
  }

  private void createMimeMultiparts(MimeMessage message) throws MessagingException {
    MimeMultipart rootMixedMultipart = new MimeMultipart("mixed");
    message.setContent(rootMixedMultipart);

    MimeMultipart nestedRelatedMultipart = new MimeMultipart("related");
    MimeBodyPart relatedBodyPart = new MimeBodyPart();
    relatedBodyPart.setContent(nestedRelatedMultipart);
    rootMixedMultipart.addBodyPart(relatedBodyPart);
  }

  public void setFrom(String from) throws MessagingException {
    this.setFrom(InternetAddress.parse(from)[0]);
  }

  public void setFrom(InternetAddress from) throws MessagingException {
    this.validateAddress(from);
    this.message.setFrom(from);
  }

  public void setTo(String to) throws MessagingException {
    this.setTo(InternetAddress.parse(to));
  }

  public void setTo(InternetAddress[] to) throws MessagingException {
    this.validateAddresses(to);
    this.message.setRecipients(RecipientType.TO, to);
  }

  public void setCc(String cc) throws MessagingException {
    this.setCc(InternetAddress.parse(cc));
  }

  public void setCc(InternetAddress[] cc) throws MessagingException {
    this.validateAddresses(cc);
    this.message.setRecipients(RecipientType.CC, cc);
  }

  public void setBcc(String bcc) throws MessagingException {
    this.setCc(InternetAddress.parse(bcc));
  }

  public void setBcc(InternetAddress[] bcc) throws MessagingException {
    this.validateAddresses(bcc);
    this.message.setRecipients(RecipientType.BCC, bcc);
  }

  public void setReplyTo(InternetAddress[] replyTo) throws MessagingException {
    this.validateAddresses(replyTo);
    this.message.setReplyTo(replyTo);
  }

  public void setReplyTo(InternetAddress replyTo) throws MessagingException {
    this.setReplyTo(new InternetAddress[]{replyTo});
  }

  public void setSubject(String subject) throws MessagingException {
    this.message.setSubject(subject, encoding);
  }

  public void setText(String text) throws MessagingException {
    this.setText(text, false);
  }

  public void setText(String text, boolean isHtml) throws MessagingException {
    if (isHtml) {
      this.message.setText(text, this.encoding, "html");
    } else {
      this.message.setText(text, this.encoding);
    }
  }

  public void setEncoding(String encoding) {
    this.encoding = encoding;
  }

  public MimeMessage getMimeMessage() {
    return this.message;
  }

  public boolean isValidateAddresses() {
    return this.validateAddresses;
  }

  public void setValidateAddresses(boolean validateAddresses) {
    this.validateAddresses = validateAddresses;
  }

  private void validateAddress(InternetAddress address) throws AddressException {
    if (isValidateAddresses()) {
      address.validate();
    }
  }

  private void validateAddresses(InternetAddress[] addresses) throws AddressException {
    for (InternetAddress address : addresses) {
      validateAddress(address);
    }
  }
}
