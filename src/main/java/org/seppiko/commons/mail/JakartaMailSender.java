/*
 * Copyright 2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.mail;

import jakarta.mail.MessagingException;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.MimeMessage;
import java.time.Clock;
import java.util.Date;

/**
 * A jakarta mail send helper
 *
 * @see <a href="https://www.javadoc.io/doc/org.eclipse.angus/angus-mail/latest/com.sun.mail/com/sun/mail/smtp/package-summary.html">SMTP properties</a>
 * @author Leonard Woo
 */
public class JakartaMailSender {

  private Session session;

  private String username;

  private String password;

  private void setSession(Session session) {
    this.session = session;
  }

  /**
   * Set username and password
   *
   * @param username Username
   * @param password Password
   */
  public void setUser(String username, String password) {
    this.username = username;
    this.password = password;
  }

  /**
   * Sender initialization
   *
   * @param session Jakarta Mail Session
   */
  public JakartaMailSender(Session session) {
    this.setSession(session);
  }

  /**
   * MimeMessageHelper initialization utility
   *
   * @return MimeMessageHelper
   * @throws MessagingException initialization failed
   */
  public MimeMessageHelper createMimeMessage() throws MessagingException {
    return new MimeMessageHelper(new MimeMessage(session));
  }

  /**
   * Send mail with MailMessage
   *
   * @param message Mail Message
   * @throws MessagingException MIME Mail Message exception
   * @throws IllegalAccessException Data check exception
   */
  public void send(MailMessage message)
      throws MessagingException, IllegalAccessException {
    MimeMessageHelper helper = createMimeMessage();
    if (message.getFrom() == null || message.getTo() == null
        || message.getSubject() == null || message.getText() == null) {
      throw new IllegalAccessException("From \\ To \\ Subject and Text must be NOT null");
    }
    String encoding = String.valueOf(session.getProperties().get("mail.encoding"));
    helper.setEncoding(encoding);
    helper.setFrom(message.getFrom());
    helper.setTo(message.getTo());
    if (message.getCc() != null) {
      helper.setCc(message.getCc());
    }
    if (message.getBcc() != null) {
      helper.setBcc(message.getBcc());
    }
    if (message.getReplyTo() != null) {
      helper.setReplyTo(message.getReplyTo());
    }
    helper.setSubject(message.getSubject());
    helper.setText(message.getText());
    send(helper.getMimeMessage());
  }

  /**
   * Send mail
   *
   * @param message MIME Message
   * @throws MessagingException failed to establish transmission object or send
   * @throws IllegalAccessException session is null
   */
  public void send(MimeMessage message) throws MessagingException, IllegalAccessException {
    if (session == null) {
      throw new IllegalAccessException("Session must be not NULL");
    }

    Transport trans = session.getTransport();

    if ("".equals(username)) {
      username = null;
      if ("".equals(password)) {
        password = null;
      }
    }
    trans.connect(username, password);

    if (message.getSentDate() == null) {
      message.setSentDate( new Date( Clock.systemUTC().millis() ) );
    }

    String messageId = message.getMessageID();
    message.saveChanges();
    if (messageId != null) {
      message.setHeader("Message-ID", messageId);
    }

    trans.sendMessage(message, message.getAllRecipients());
    trans.close();
  }

}
