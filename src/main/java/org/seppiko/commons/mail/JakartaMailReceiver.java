/*
 * Copyright 2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.mail;

import jakarta.mail.Address;
import jakarta.mail.FetchProfile;
import jakarta.mail.Flags;
import jakarta.mail.Flags.Flag;
import jakarta.mail.Folder;
import jakarta.mail.Message;
import jakarta.mail.Message.RecipientType;
import jakarta.mail.MessagingException;
import jakarta.mail.Session;
import jakarta.mail.Store;
import jakarta.mail.UIDFolder.FetchProfileItem;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeUtility;
import jakarta.mail.search.FlagTerm;
import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * A jakarta mail receive helper
 *
 * @see <a href="https://www.javadoc.io/doc/org.eclipse.angus/angus-mail/latest/com.sun.mail/com/sun/mail/imap/package-summary.html">IMAP properties</a>
 * @see <a href="https://www.javadoc.io/doc/org.eclipse.angus/angus-mail/latest/com.sun.mail/com/sun/mail/pop3/package-summary.html">POP3 properties</a>
 * @author Leonard Woo
 */
public class JakartaMailReceiver {

  private Session session;

  private String protocol;

  private String username;

  private String password;

  private void setSession(Session session) {
    this.session = session;
    this.protocol = session.getProperty("mail.store.protocol");
  }

  /**
   * Set username and password
   *
   * @param username Username
   * @param password Password
   */
  public void setUser(String username, String password) {
    this.username = username;
    this.password = password;
  }

  /**
   * Receiver initialization
   *
   * @param session Jakarta Mail Session
   */
  public JakartaMailReceiver(Session session) {
    this.setSession(session);
  }

  /**
   * Receive mail and mark read
   *
   * @param folderName mail folder,set INBOX is default
   * @return mail message list
   * @throws IllegalAccessException session is null
   * @throws MessagingException mail exception
   * @throws IOException parser exception
   */
  public ArrayList<MailMessage> receive(String folderName)
      throws IllegalAccessException, MessagingException, IOException {
    if (session == null) {
      throw new IllegalAccessException("Session must be not NULL");
    }

    Store store = session.getStore();
    store.connect(this.username, this.password);

    Folder folder = getFolder(store, folderName);
    Message[] messages = folder.getMessages();

    ArrayList<MailMessage> mails = new ArrayList<>();
    for (Message message: messages) {
      mails.add( parseMailMessage(message) );
      message.setFlag(Flags.Flag.SEEN, true);
    }

    folder.close();
    store.close();

    return mails;
  }

  /*
   * Folder.HOLDS_FOLDERS   This folder can contain other folders.
   * Folder.HOLDS_MESSAGES  This folder can contain messages.
   * Folder.READ_ONLY       The Folder is read only.
   * Folder.READ_WRITE      The state and contents of this folder can be modified.
   */
  /*
   * Flags.Flag.ANSWERED This message has been answered.
   * Flags.Flag.DELETED  This message is marked deleted.
   * Flags.Flag.DRAFT    This message is a draft.
   * Flags.Flag.FLAGGED  This message is flagged.
   * Flags.Flag.RECENT   This message is recent. New message
   * Flags.Flag.SEEN     This message is seen. Message have read
   * Flags.Flag.USER     A special flag that indicates that this folder supports user defined flags.
   */
  private Folder getFolder(Store store, String folderName) throws MessagingException {

    FetchProfile profile = new FetchProfile();
    profile.add(FetchProfileItem.UID);

    Folder inbox = store.getFolder(folderName);
    Flag flag;
    if (this.protocol.startsWith("imap")) {
      inbox.open(Folder.READ_WRITE);
      flag = Flag.RECENT;
    } else if (this.protocol.startsWith("pop3")) {
      inbox.open(Folder.READ_ONLY);
      flag = Flag.SEEN;
    } else {
      throw new IllegalArgumentException("Jakarta Mail illegal store protocol");
    }

    FlagTerm ft = new FlagTerm(new Flags(flag), false);
    Message[] messages = inbox.search(ft);
    inbox.fetch(messages, profile);
    return inbox;
  }

  // parser and convert message to mailmessage entity
  private MailMessage parseMailMessage(Message message)
      throws MessagingException, IOException {
    MailMessage mail = new MailMessage();

    String from = MimeUtility.decodeText(message.getFrom()[0].toString());
    mail.setFrom(new InternetAddress(from));

    Address[] recipients = message.getAllRecipients();
    if (recipients != null) {
      ArrayList<InternetAddress> toIA = new ArrayList<>();
      ArrayList<InternetAddress> ccIA = new ArrayList<>();
      ArrayList<InternetAddress> bccIA = new ArrayList<>();
      for (Address recipient : recipients) {
        if (recipient == null) {
          continue;
        }
        if (RecipientType.TO.toString().equals(recipient.getType())) {
          String to = MimeUtility.decodeText(recipient.toString());
          toIA.add(new InternetAddress(to));
        }
        if (RecipientType.CC.toString().equals(recipient.getType())) {
          String cc = MimeUtility.decodeText(recipient.toString());
          ccIA.add(new InternetAddress(cc));
        }
        if (RecipientType.BCC.toString().equals(recipient.getType())) {
          String bcc = MimeUtility.decodeText(recipient.toString());
          bccIA.add(new InternetAddress(bcc));
        }
      }
      if (toIA.size() > 0) {
        mail.setTo(convert(toIA));
      }
      if (ccIA.size() > 0) {
        mail.setCc(convert(ccIA));
      }
      if (bccIA.size() > 0) {
        mail.setBcc(convert(bccIA));
      }
    }

    String replyTo = MimeUtility.decodeText(message.getReplyTo()[0].toString());
    mail.setReplyTo(new InternetAddress(replyTo));


    ZoneId zoneId = ZoneId.of("UTC");

    ZonedDateTime sent = message.getSentDate().toInstant().atZone(zoneId);
    mail.setSentTimestamp(sent);

    ZonedDateTime received = message.getReceivedDate().toInstant().atZone(zoneId);
    mail.setReceivedTimestamp(received);

    mail.setSubject(message.getSubject());
    mail.setText( String.valueOf(message.getContent()) );

    return mail;
  }

  // convert list to array
  @SuppressWarnings("unchecked")
  private <T> T[] convert(List<T> list) {
    T[] ts = (T[]) new Object[list.size()];
    for (int i = 0; i < list.size(); i++) {
      ts[i] = list.get(i);
    }
    return ts;
  }
}
