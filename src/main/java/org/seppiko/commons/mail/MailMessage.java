/*
 * Copyright 2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.commons.mail;

import jakarta.mail.internet.InternetAddress;
import java.time.ZonedDateTime;

/**
 * A model for Jakarta Mail
 *
 * @author Leonard Woo
 */
public class MailMessage {

  public MailMessage() {
  }

  private InternetAddress from;

  private InternetAddress[] to;

  private InternetAddress[] cc;

  private InternetAddress[] bcc;

  private InternetAddress replyTo;

  private ZonedDateTime sentTimestamp;

  private ZonedDateTime receivedTimestamp;

  private String subject;

  private String text;

  public InternetAddress getFrom() {
    return from;
  }

  public void setFrom(InternetAddress from) {
    this.from = from;
  }

  public InternetAddress[] getTo() {
    return to;
  }

  public void setTo(InternetAddress[] to) {
    this.to = to;
  }

  public InternetAddress[] getCc() {
    return cc;
  }

  public void setCc(InternetAddress[] cc) {
    this.cc = cc;
  }

  public InternetAddress[] getBcc() {
    return bcc;
  }

  public void setBcc(InternetAddress[] bcc) {
    this.bcc = bcc;
  }

  public InternetAddress getReplyTo() {
    return replyTo;
  }

  public void setReplyTo(InternetAddress replyTo) {
    this.replyTo = replyTo;
  }

  public ZonedDateTime getSentTimestamp() {
    return sentTimestamp;
  }

  public void setSentTimestamp(ZonedDateTime sentTimestamp) {
    this.sentTimestamp = sentTimestamp;
  }

  public ZonedDateTime getReceivedTimestamp() {
    return receivedTimestamp;
  }

  public void setReceivedTimestamp(ZonedDateTime receivedTimestamp) {
    this.receivedTimestamp = receivedTimestamp;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }
}
