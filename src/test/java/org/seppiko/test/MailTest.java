/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.test;

import java.util.Properties;
import org.junit.jupiter.api.Test;

/**
 * @author Leonard Woo
 */
public class MailTest {

  private final Properties props = new Properties();

  private final String username = "lawson.purdy@ethereal.email";
  private final String password = "j61daQF7Ab4sHWWV1C";

//  @BeforeEach
  public void initProps() {
    props.put("mail.encoding", "UTF-8"); // Default value

    props.put("mail.transport.protocol", "smtp");
    props.put("mail.smtp.host", "smtp.ethereal.email");
    props.put("mail.smtp.port", 587);

    props.put("mail.smtps.auth", true);
    props.put("mail.smtp.ssl.enable", false);
    props.put("mail.smtp.starttls.enable", true);
    props.put("mail.smtp.starttls.required", true);
//    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//    props.put("mail.smtp.socketFactory.fallback", false);

//    props.put("mail.smtps.socks.host", "127.0.0.1");
//    props.put("mail.smtps.socks.port", 1080);

    props.put("mail.smtps.connectiontimeout", 5*1000);
    props.put("mail.smtps.timeout", 3*1000);
    props.put("mail.smtps.writetimeout", 5*1000);

    props.put("mail.store.protocol", "imaps");
    props.put("mail.imaps.host", "imap.ethereal.email");
    props.put("mail.imaps.port", 993);

    props.put("mail.imaps.auth", true);
    props.put("mail.imap.ssl.enable", true);
    props.put("mail.imap.starttls.enable", true);
    props.put("mail.imap.starttls.required", true);
//    props.put("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//    props.put("mail.imap.socketFactory.fallback", false);

//    props.put("mail.imaps.socks.host", "127.0.0.1");
//    props.put("mail.imaps.socks.port", 1080);

    props.put("mail.imaps.connectiontimeout", 5*1000);
    props.put("mail.imaps.timeout", 3*1000);
    props.put("mail.imaps.writetimeout", 5*1000);

  }

  @Test
  public void jakartaMailSendTest() throws Throwable {
//    Session session = Session.getInstance(props);
//    JakartaMailSender sender = new JakartaMailSender(session);
//    sender.setUser(username, password);
//    MailMessage message = new MailMessage();
//    message.setFrom(new InternetAddress(username));
//    message.setTo(new InternetAddress[]{new InternetAddress("leo_916@yahoo.com")});
//    message.setSubject("Seppiko Commons Mail Test");
//    message.setText("Seppiko Commons Mail 1.1 with Jakarta mail 2.1.0 test");
//    sender.send(message);
  }

  @Test
  public void jakartaMailReceiveTest() throws Throwable {
//    Session session = Session.getInstance(props);
//    JakartaMailReceiver receive = new JakartaMailReceiver(session);
//    receive.setUser(username, password);
//    ArrayList<MailMessage> messages = receive.receive("INBOX");
//    for (MailMessage message: messages) {
//      // read mail content
//      System.out.println("From " + message.getFrom());
//      System.out.println("To " + Arrays.toString(message.getTo()));
//      System.out.println("Cc " + Arrays.toString(message.getCc()));
//      System.out.println("Bcc " + Arrays.toString(message.getBcc()));
//      System.out.println("ReplyTo " + message.getReplyTo());
//      System.out.println("Date " + message.getReceivedTimestamp().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
//      System.out.println("Subject " + message.getSubject());
//      System.out.println(message.getText());
//    }
  }
}
